@extends('adminlte.master')

@section('content')
<div class="ml-2 mt-2">
<div class="card card-primary">
                <div class="card-header">
                    <h3 class="card-title">New Cast</h3>
                </div>
                <!-- /.card-header -->
                <!-- form start -->
                <form role="form" action="/cast" method="POST">
                    @csrf
                    <div class="card-body">
                    <div class="form-group">
                        <label for="nama">Name</label>
                        <input type="text" class="form-control" id="nama" name="nama" value="{{old('nama','')}}" placeholder="Enter name" required>
                        @error('nama')
                            <div class="alert alert-danger"> {{ $message }} </div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="umur">Age</label>
                        <input type="integer" class="form-control" id="umur" name="umur" value="{{old('umur','')}}" placeholder="Enter age" required>
                        @error('umur')
                            <div class="alert alert-danger"> {{ $message }} </div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="bio">Bio</label>
                        <input type="text" class="form-control" id="bio" name="bio" value="{{old('bio','')}}" placeholder="Enter bio" required>
                        @error('bio')
                            <div class="alert alert-danger"> {{ $message }} </div>
                        @enderror
                    </div>
                    <!-- /.card-body -->

                    <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Create</button>
                    </div>
                </form>
                </div>
</div>

@endsection