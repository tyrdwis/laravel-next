@extends('adminlte.master')

@section('content')
<div class="ml-2 mt-2">
<div class="card">
              <div class="card-header">
                <h3 class="card-title">Cast List</h3>

                <div class="card-tools">
                  <div class="input-group input-group-sm" style="width: 150px;">
                    <input type="text" name="table_search" class="form-control float-right" placeholder="Search">

                    <div class="input-group-append">
                      <button type="submit" class="btn btn-default">
                        <i class="fas fa-search"></i>
                      </button>
                    </div>
                  </div>
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body table-responsive p-0">
                @if(session('success'))
                  <div class="alert alert-success">
                    {{session('success')}}
                  </div>
                @endif
                <table class="table table-hover text-nowrap">
                  <thead>
                    <tr>
                      <th>ID</th>
                      <th>Name</th>
                      <th>Age</th>
                      <th>Bio</th>
                      <th>Detail</th>
                    </tr>
                  </thead>
                  <tbody>
                    @forelse($casts as $key => $cast)
                      <tr>
                        <td>{{$key +1 }}</td>
                        <td>{{$cast ->nama}}</td>
                        <td>{{$cast ->umur}}</td>
                        <td>{{$cast ->bio}}</td>
                        <td>
                          <a href="/cast/{{$cast->id}}" class="btn btn-primary btn-sm">show</a>
                          <a href="/cast/{{$cast->id}}/edit" class="btn btn-warning btn-sm">edit</a>
                          <form action="/cast/{{$cast->id}}" method="post">
                          <input type="submit" value="delete" class="btn btn-danger btn-sm">
                          @csrf
                          @method('DELETE')
                        </form>
                        </td>
                      </tr>
                    @empty
                    <tr>
                      <td colspan="4" align="center"> No Cast </td>
                    </tr>
                    @endforelse
                  </tbody>
                </table>
                <a class="btn btn-primary mb-2 ml-2" href="/cast/create">Add New Cast</a>
              </div>
              <!-- /.card-body -->
            </div>
</div>
@endsection